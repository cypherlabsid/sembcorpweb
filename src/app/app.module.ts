import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './register/register.component';
import { SettingsComponent } from './settings/settings.component';
import { TagsComponent } from './tags/tags.component';
import { ContactReportsComponent } from './contact-reports/contact-reports.component';
import { HomeComponent } from './home/home.component';
import { VerifyCodeComponent } from './verify-code/verify-code.component';
import { DahsboardComponent } from './dahsboard/dahsboard.component';
import { RecentAddComponent } from './recent-add/recent-add.component';
import { ViewReportComponent } from './view-report/view-report.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule }    from '@angular/forms';
import {HttpClientModule} from "@angular/common/http";
import { CoreModule } from './core/core.module';
import {FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { Select2Module } from 'ng2-select2';

import {AuthService} from './service/auth.service';
import {ContactReportsService} from './service/contact-reports.service';
import {ForgetPasswordService} from './service/forget-password.service';
import {HomeService} from './service/home.service';
import {RegisterService} from './service/register.service';
import {SettingsService} from './service/settings.service';
import {TagsService} from './service/tags.service';


import {DataTableModule} from "angular-6-datatable";
import { EditUserComponent } from './edit-user/edit-user.component';

import { AddTagsComponent } from './tags/add-tags.component';
import { EditTagsComponent } from './tags/edit-tags.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {DataFilterPipe} from "./data-filter.pipe";
import { AngularFileUploaderModule } from "angular-file-uploader";
import { AuthConfig } from './auth-config';

@NgModule({

  declarations: [
    AppComponent,
    LoginComponent,
    UpdatePasswordComponent,
    ForgotPasswordComponent,
    RegisterComponent,
    SettingsComponent,
    TagsComponent,
    ContactReportsComponent,
    HomeComponent,
    EditUserComponent,
    AddTagsComponent,
    EditTagsComponent,
    VerifyCodeComponent,
    DahsboardComponent,
    RecentAddComponent,
    ViewReportComponent,
    DataFilterPipe,
    ResetPasswordComponent,
    AuthConfig
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    DataTableModule,
    NgbModule.forRoot(),
    MatTabsModule,
    DropzoneModule,
    Select2Module,
    AngularFileUploaderModule
  ],
  providers: [AuthService,ContactReportsService,ForgetPasswordService,
  HomeService,RegisterService,SettingsService,TagsService,AuthConfig],
  bootstrap: [AppComponent]
})
export class AppModule { }
