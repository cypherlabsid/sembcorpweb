import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { ContactReportsService } from '../service/contact-reports.service';
import { IMultiSelectOption, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  NgbDateAdapter, NgbDateNativeAdapter } from '@ng-bootstrap/ng-bootstrap';
import toastr from 'toastr';
import 'toastr/build/toastr.min.css';

@Component({
    selector: 'app-view-report',
    templateUrl: './view-report.component.html',
    styleUrls: ['./view-report.component.css'],
    providers: [{
        provide: NgbDateAdapter,
        useClass: NgbDateNativeAdapter
    }]
})
export class ViewReportComponent implements OnInit {
    myOptions: IMultiSelectOption[];
    mySettings: IMultiSelectSettings = {
        enableSearch: true,
        buttonClasses: 'btn btn-primary btn-block',
        maxHeight: '300px;',
        dynamicTitleMaxItems: 5,
        containerClasses: 'dropdown-inline',
        displayAllSelectedText: true

    };

    title = 'View Contact Report';
    id: number;
    private sub: any;
    private data: any;
    private items: any;
    company: string;
    date: string;
    text_report: any;
    text_tags: any;
    selectedItems: string;
    public tagsList: any;
    isLoading = true
    private tgl_get: string;

     title_report: string;
     marketer: string;
     purpose: string;
    public value: string[];
    submitted = false;
    model: Date;
    recentView: any

    constructor(
        private router: ActivatedRoute,
        private route:Router,
        private contact_service: ContactReportsService,
        private formBuilder: FormBuilder) {}

    contact: FormGroup;

    recentViews: any

    ngOnInit() {

        this.contact = this.formBuilder.group({
            title: ['', Validators.required],
            purpose: ['', Validators.required],
            marketer: ['', Validators.required]
        });

       this.loadData()
    }


    loadData() {
      this.sub = this.router.params.subscribe(params => {
        this.id = +params['id'];
        this._setRecent(params['id'])

        this.contact_service.view_report(this.id).subscribe(
          res => {

            this.items = res.body['data'];
            this.data = this.items;


            this.title_report = this.data['title'];
            this.marketer = this.data['marketer'];
            this.purpose = this.data['purpose'];
            const newline = String.fromCharCode(13, 10);
            this.text_report = this.data['image'];
            this.model = new Date(this.data.date)

            this.contact.patchValue({
              title : this.data['title'],
              marketer : this.data['marketer'],
              purpose : this.data['purpose']
            })

            try {
              this.value = this.data['tags'].split(",")
            } catch (e) {
              this.value = [];
            }

          }, error => {

            if (!error['error']['status']) {
              this.route.navigate(['/home']);
            }
          });
        this.contact_service.getTags().subscribe(
          res => {
            this.tagsList = res.body;
            this.isLoading = false
          }, error => {
            console.log(error);
          });

      });
    }


    checkRecent() {
        if (typeof localStorage['_recent'] !== 'undefined') {
            this.recentViews = JSON.parse(localStorage._recent)
            if (Array.isArray(this.recentViews)) {
              return true
            } else {
              this.recentViews = []
              return false
            }

        } else {
            this.recentViews = []
            return false
        }
    }

    removeArray(arr,a:any) {
        var what, a:any = arguments, L = a.length, ax;
        while (L > 1 && arr.length) {
            what = a[--L];
            while ((ax= arr.indexOf(what)) !== -1) {
                arr.splice(ax, 1);
            }
        }
        return arr;
    }

    formatDate(date) {
      try {
        let d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
      } catch (e) {
        let d = new Date(),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();
        return [year, month, day].join('-');
      }

    }

    ReplaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    onItemSelect(data: {
        value: string[]
    }) {
        this.selectedItems = data.value.join(',')
    }


    select(model) {
        this.model = new Date(model)
    }

    update() {
        this.submitted = true;

        this.contact.value.tags = this.selectedItems
        this.contact.value.id = this.id
        this.contact.value.tgl = this.formatDate(this.model)
        this.isLoading = true
        this.contact_service.update_report(this.contact.value).subscribe(
            data => {
                this.loadData()
                toastr.success('Success', data["message"])
              this.isLoading = false
            }
        )

    }

    _setRecent(id_view:any){
        if(this.checkRecent()){

            let found_key = undefined;
            if (Array.isArray(this.recentViews)) {
              found_key = this.recentViews.find(function(element) {
                return element == id_view;
              });
            }


            if(typeof found_key === 'undefined'){
                this.recentViews.push(id_view);
                localStorage.setItem('_recent',JSON.stringify(this.recentViews))
            }else{
                this.recentViews = this.removeArray(this.recentViews,id_view)
                this.recentViews.push(id_view);
                localStorage.setItem('_recent',JSON.stringify(this.recentViews))
            }

            if(this.recentViews.length >= 20){
                this.recentViews = this.recentViews.shift();
                localStorage.setItem('_recent', JSON.stringify(this.recentViews));
            }

        }else{
            this.recentViews.push(id_view)
            localStorage.setItem('_recent',JSON.stringify(this.recentViews))
        }
    }

}
