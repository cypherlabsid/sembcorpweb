import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import 	toastr from 'toastr';
import 'toastr/build/toastr.min.css';

import {ForgetPasswordService} from "../service/forget-password.service";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
	  forgotForm: FormGroup;
    loading = false;
    submitted = false;
    isLoading = false;

  constructor(private formBuilder: FormBuilder,private router: Router, private forgetService: ForgetPasswordService) { }

  ngOnInit() {
  	
  	this.forgotForm = this.formBuilder.group({
             email: ['', Validators.required],
        });
    
  }

   get f() { return this.forgotForm.controls; }


    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.forgotForm.invalid) {
            return;
        }

        this.isLoading = true;

       this.forgetService.verify_email(this.forgotForm.value.email).pipe(first()).pipe(first())
      .subscribe(
        data => {
          this.isLoading = false;
          toastr.success(data.body['message'])
        },
        error => {
          this.isLoading = false;
          toastr.success(error.error['message'])
        });

    }

}
