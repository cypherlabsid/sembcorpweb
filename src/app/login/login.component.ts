import { Component, OnInit } from '@angular/core';


import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from "../service/auth.service";
import {first} from "rxjs/operators";
import { AuthConfig } from '../auth-config'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ AuthConfig]
})
export class LoginComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthService, private authConf:AuthConfig) { }

  loginForm: FormGroup;
  submitted: boolean = false;
  invalidLogin: boolean = false;
  loading = false;
  item:string;
  error_message:string;

  ngOnInit() {
   this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(5)]]
        });

    if(this.authConf.isLoggedin()){
        this.router.navigate(['/home']);
    }

  }

  get f() {
    return this.loginForm.controls;

  }

  login(){
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    
   
    this.authService.login(this.loginForm.value.username, this.loginForm.value.password)
            .subscribe(
                (datac:any) => {
	                if (typeof datac.body.data != "undefined") {
					   this.item = JSON.stringify(datac.body.data[0]);
	                    localStorage.setItem('token', this.item);
	                    this.router.navigate(['/home']);
					}
                    
                },
                error => {
                    this.error_message = error.error.message;
                });
  }


  register() {  
     this.router.navigate(["/register"]);
  }
  

}
