import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentAddComponent } from './recent-add.component';

describe('RecentAddComponent', () => {
  let component: RecentAddComponent;
  let fixture: ComponentFixture<RecentAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
