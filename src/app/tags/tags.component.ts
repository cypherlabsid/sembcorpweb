import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Tags} from "../model/tags.model";
import {TagsService} from "../service/tags.service";
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import 	toastr from 'toastr';
import 'toastr/build/toastr.min.css';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {
  public data : any;
  private item:any;
  title = 'Search Tags';
  private values = '';
  public tagsList:any ;
  closeResult: string;
  modalRef:any;
  name_tags:string;
  private temp_id:string;

  constructor(private formBuilder: FormBuilder, private tags_service : TagsService, private router:Router, private modalService: NgbModal) { }

  tagsForm: FormGroup;
  tagsInput: FormGroup;
  tagsUpdate: FormGroup;
  submitted: boolean = false;
  invalidTags: boolean = false;
  loading = false;
  isLoading = true
  public filterQuery = "";

  ngOnInit() {
       this.tagsInput = this.formBuilder.group({
            tags: ['', Validators.required]
        });

        this.tagsUpdate = this.formBuilder.group({
            tag: ['', Validators.required]
        });
 
  		this.tagsForm = this.formBuilder.group({
            tags: ['', Validators.required]
        });
        
        this.tags_service.tagsLoad().subscribe(res => {
	       
	        if(typeof res === 'object' && res !== null){
		        
		        this.data = res["data"] 
	        }
          this.isLoading = false
        },error=>{
          console.log(error);
        });

  }

   get f() { return this.tagsForm.controls; }

   onKey(value: string){
      this.values = value;

       this.tags_service.searchTags(this.values).subscribe(res => {
          console.log("Datas:  "+res["data"]);
          this.item = res["data"];
          this.data = this.item;
        },error=>{
          console.log(error);
        });
   }

  open(content) {

	this.modalRef = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  open_udpate(content, id:string, tags:string){
    var test = { tags_id: ""+id, tags_name: ""+tags }​​​​​​​;

    localStorage.removeItem("tags");
    localStorage.setItem("tags", JSON.stringify(test));
    this.name_tags = tags;

    this.modalRef = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

   insert(){
	   this.isLoading = true
   		this.tags_service.input_tags(this.tagsInput.value.tags).subscribe(
       data => {
            this.ngOnInit();
            this.isLoading = false
            this.modalRef.close()
            toastr.success('Success', data["message"])
        },
        error => {
	     
	        this.isLoading = false
            console.log(error)
            this.modalRef.close()
        });
   }

   delete(){
      this.tags_service.deleteTags(this.temp_id).subscribe(
       data => {
            this.ngOnInit();
            this.modalRef.close()
            toastr.success('Success', data["message"]);
        },
        error => {
            console.log(error);
            this.modalRef.close()
        });
   }

   confirm_delete(confirm, id:string) {
    this.modalRef = this.modalService.open(confirm, {ariaLabelledBy: 'modal-basic-title'})
    this.temp_id = id;
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

   update(){
   
      this.isLoading = true
      this.tags_service.update_tags(this.tagsUpdate.value.tag).subscribe(
       data => {

            this.ngOnInit();
            this.isLoading = false
            this.modalRef.close()
            toastr.success('Success', data["message"])
        },
        error => {
       
          this.isLoading = false
            
            this.modalRef.close()
        });
   }


}
