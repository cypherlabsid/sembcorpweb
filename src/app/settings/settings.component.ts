import { Component, OnInit } from '@angular/core';
import {SettingsService} from "../service/settings.service";
import {Router} from '@angular/router';
import {User} from "../model/user.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
	public data : any;
  title = 'Account Information';
  title_update = 'Setting Account';
  private item:any;
  public data_user:string[];
  myItem = localStorage.getItem('token');
  editForm: FormGroup;



  constructor(private settings_service : SettingsService, private router:Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
      this.editForm = this.formBuilder.group({
      id: [],
      username:['', Validators.required],
      email: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      
    });

      this.data_user = [JSON.parse(this.myItem)['picture'].toString(),
                      JSON.parse(this.myItem)['first_name'].toString(),
                      JSON.parse(this.myItem)['last_name'].toString(),
                      JSON.parse(this.myItem)['username'].toString(),
                      JSON.parse(this.myItem)['email'].toString() ];
  }	

   onSubmit() {
    this.settings_service.updateUser(this.editForm.value.first_name,this.editForm.value.last_name).pipe(first()).pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/dahsboard', {outlets:{first: ['settings']}}]);
        },
        error => {
            console.log(error);
        });
  }

}
