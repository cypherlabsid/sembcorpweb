import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {RegisterService} from "../service/register.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	  registerForm: FormGroup;
    loading = false;
    submitted = false;

  constructor(private formBuilder: FormBuilder,private router: Router, private regService: RegisterService) { }

  ngOnInit() {
  this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            username: ['', Validators.required],
            email: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
  }

   get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.regService.register(this.registerForm.value.firstName, this.registerForm.value.lastName, this.registerForm.value.username, this.registerForm.value.password,this.registerForm.value.email)
            .subscribe(
                datac => {
                    this.router.navigate(['/login']);
                },
                error => {
                    alert(error);
                });
    
    }
    
}
