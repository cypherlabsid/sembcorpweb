import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import 	toastr from 'toastr';
import 'toastr/build/toastr.min.css';
import { first } from 'rxjs/operators';

import { ForgetPasswordService } from '../service/forget-password.service';


@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
	
  tokenValid = true
  resetPassword : FormGroup;
  submitted = false
  isLoading	= false
  _isSuccess = false;

  constructor(
  		private route: ActivatedRoute, 
  		private formBuilder: FormBuilder,
  		private router: Router,
  		private service : ForgetPasswordService) { }

  ngOnInit() {
	  const token: string = this.route.snapshot.queryParamMap.get('token')
	  
	  if(token == null || token.length <= 10){
		  this.tokenValid = false
	  }
	  
	  this.resetPassword = this.formBuilder.group({
            newPassword: ['', [Validators.required, Validators.minLength(6)]],
			confirmPassword : ['', [Validators.required]]
        },{validator: this.checkPasswords });

  }
  
  get f() { return this.resetPassword.controls }
  
  onSubmit() {
        this.submitted = true
       

        if (this.resetPassword.invalid) {
            return;
        }else{
	        this.isLoading = true
	        //setTimeout(() => { toastr.success('Have fun storming the castle!', 'Miracle Max Says');this.isLoading=false;}, 3000)
	        
	        let data = {
		        new_password : this.resetPassword.controls.newPassword.value,
		        confirm_password: this.resetPassword.controls.confirmPassword.value
	        }
	        
	       
        }
       
    }
    
    checkPasswords(group: FormGroup) { 
	  let pass = group.controls.newPassword.value;
	  let confirmPass = group.controls.confirmPassword.value;
	
	  return pass === confirmPass ? null : { notSame: true }     
	}

}
