import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthConfig } from '../auth-config';


@Component({
  selector: 'app-dahsboard',
  templateUrl: './dahsboard.component.html',
  styleUrls: ['./dahsboard.component.css']
})
export class DahsboardComponent implements OnInit {
  public get_name_user:string;
  myItem = localStorage.getItem('token');
  constructor(public authConf: AuthConfig,private router: Router) {
    if (!this.authConf.isLoggedin()) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
  	this.get_name_user = JSON.parse(this.myItem)['email'].toString()
  }

}
