import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";
import {isNull} from 'util';

@Pipe({
    name: "dataFilter"
})
export class DataFilterPipe implements PipeTransform {

    transform(array: any[], query: string): any {
        if (query) {
            if(array.length > 0){
              let d = array[0];
              if(d.hasOwnProperty('title') && d.hasOwnProperty('marketer')){
                return _.filter(array, row=> {

                    if(row.title){
                      if(row.title.toLowerCase().indexOf(query.toLowerCase()) > -1){
                        return true
                      }
                      if(row.marketer){
                        if(row.marketer.toLowerCase().indexOf(query.toLowerCase()) > -1){
                          return true
                        }
                      }
                      let tags = false;
                      for(let k of row.tags){

                        if(k.toLowerCase().indexOf(query.toLowerCase()) > -1){
                          tags = true;
                        }
                      }

                      return tags;
                    }
                    return false;
                });
              }else{
                return _.filter(array, row=> {
                  if(row.name.indexOf(query) > -1){
                    return true
                  }
                  return false
                });
              }
            }


        }
        return array;
    }
}
