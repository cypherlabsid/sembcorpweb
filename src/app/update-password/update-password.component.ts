import { Component, OnInit } from '@angular/core';


import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators,FormGroupDirective, NgForm, FormControl } from '@angular/forms';
import {AuthService} from '../service/auth.service';
import {first} from 'rxjs/operators';
import { AuthConfig } from '../auth-config';
import { ForgetPasswordService} from '../service/forget-password.service';
import 	toastr from 'toastr';
import 'toastr/build/toastr.min.css';



@Component({
  selector: 'update-password',
  templateUrl: './update-password.html',
  styleUrls: ['./style.css'],
  providers: [ AuthConfig]
})
export class UpdatePasswordComponent implements OnInit {

  resetForm: FormGroup;
  submitted = false;
  user_id: any;
  isLoading = false;
  error_message = "";


  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private authService: AuthService,
              private forgetService: ForgetPasswordService,
              private authConf: AuthConfig) { }

  ngOnInit(): void {

    this.user_id = this.route.snapshot.queryParams.push

    this.resetForm = this.formBuilder.group({
      new_password: ['', Validators.required],
      confirm_password: ['']
    },{ validator: this.checkPasswords });

    if ( this.authConf.isLoggedin() ) {
      this.router.navigate(['/home']);
    }

    if ( typeof this.user_id === 'undefined'){
      this.router.navigate(['/']);
    }
  }

  get f() { return this.resetForm.controls; }

  postReset(): void {
    this.submitted = true
    if (this.resetForm.invalid) {
      return
    }
    this.isLoading = true;
    this.forgetService.post_password(this.user_id,this.resetForm.value.new_password)
      .subscribe(
        data => {
          this.isLoading = false;
          toastr.success('Success', data.body['message']);
          console.log(data);
        },
        error => {
            this.isLoading = false;
            toastr.error('Failed', error.error['message']);
        })

  }


  checkPasswords(group: FormGroup) {
    let pass = group.controls.new_password.value;
    let confirmPass = group.controls.confirm_password.value;


    return pass === confirmPass ? null : { notSame: true }
  }




}
