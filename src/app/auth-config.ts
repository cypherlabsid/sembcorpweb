import { Pipe, Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable ()
@Pipe ({name:'conf'})
export class AuthConfig {

    constructor(private router: Router){}

    isLoggedin() {

        if (localStorage.getItem("token") === null) {
           return false;
        }

        return true;
    }

    get_user() {
        var data = localStorage.getItem("token")
        return JSON.parse(data)
    }

    logout() {
      localStorage.removeItem('token');
      this.router.navigate(['/']);

    }

}
