import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import { ContactReportsService } from '../service/contact-reports.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import toastr from 'toastr';
import 'toastr/build/toastr.min.css';


@Component({
    selector: 'app-contact-reports',
    templateUrl: './contact-reports.component.html',
    styleUrls: ['./contact-reports.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class ContactReportsComponent implements OnInit {
    title = 'Contact Reports';
    public data: string;
    private item: any;

    private item_recent: any;
    public data_recent: string;

    private item_last: any;
    public data_last: string;

    myItem = localStorage.getItem('token');
    private temp_id: string;
    closeResult: string;
    modalRef: any;
    isLoading:boolean = true;
    public filterReportAll = '';
    public recent_filter = '';
    public last_filter = '';

    constructor(
        private formBuilder: FormBuilder,
        private contact_service: ContactReportsService,
        private router: Router,
        private modalService: NgbModal) {}

    ngOnInit() {

        this.contact_service.getData()
            .subscribe(
                res => {
                    this.item = res.body['data'];
                    this.data = this.item;
                  this.isLoading = false
                }, error => {
                    console.log(error);
                });

        this.contact_service.getRecent()
            .subscribe(
                res => {
                    this.item_recent = res.body['data'];
                    this.data_recent = this.item_recent;

                }, error => {
                    console.log(error);
                });

        this.contact_service.getLast()
            .subscribe(
                res => {

                    this.item_last = res.body['data'];
                    this.data_last = this.item_last;



                }, error => {
                    console.log(error);
                    this.isLoading = false
                });

    }


    open_contact(id: string) {
        this.router.navigate(['/dahsboard', {
            outlets: {
                first: ['view-report', id]
            }
        }]);
    }

    delete_contact() {
        this.contact_service.delete(this.temp_id).subscribe(
            data => {
                this.ngOnInit();
                this.modalRef.close()
                toastr.success('Success', data['message']);
                this.router.navigate(['/dahsboard', {
                    outlets: {
                        first: ['contact-report']
                    }
                }]);
            },
            error => {
                console.log(error);
                this.modalRef.close();
            });

    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    confirm_delete(confirm, id: string) {
        this.modalRef = this.modalService.open(confirm, {
            ariaLabelledBy: 'modal-basic-title'
        })
        this.temp_id = id;
        this.modalRef.result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }


}
