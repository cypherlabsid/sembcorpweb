import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {UpdatePasswordComponent} from './update-password/update-password.component';
import {RegisterComponent} from "./register/register.component";
import {HomeComponent} from "./home/home.component";
import {ForgotPasswordComponent} from "./forgot-password/forgot-password.component";
import {ContactReportsComponent} from './contact-reports/contact-reports.component';
import {TagsComponent} from './tags/tags.component';
import {SettingsComponent} from './settings/settings.component';
import {EditUserComponent} from './edit-user/edit-user.component';
import {AddTagsComponent} from './tags/add-tags.component';
import {EditTagsComponent} from './tags/edit-tags.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component'
import {DahsboardComponent} from './dahsboard/dahsboard.component'
import {RecentAddComponent} from './recent-add/recent-add.component'
import { VerifyCodeComponent } from './verify-code/verify-code.component';
import {ViewReportComponent} from './view-report/view-report.component';

const appRoutes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'login', component: LoginComponent },
    { path: 'update-password',component:UpdatePasswordComponent},
    { path: 'register', component: RegisterComponent },
    { path: 'forget-password', component: ForgotPasswordComponent },
    { path: 'verify-code', component:VerifyCodeComponent},
    { path: 'home', component : HomeComponent},
    { path: 'dahsboard', component : DahsboardComponent, 
      children: [   { path: 'contact-report', component : ContactReportsComponent, outlet:'first',
                      children: [
                        { 

                        path: 'recent-add', component: RecentAddComponent, outlet:'second'},

                        ]},
                        
                    { path: 'search-tags', component : TagsComponent, outlet:'first'},


                    { path: 'settings', component: SettingsComponent, outlet:'first'},
                    { path: 'edit-user', component: EditUserComponent, outlet:'first'},
                    { path: 'add-tags', component: AddTagsComponent, outlet:'first'},
                    { path: 'edit-tags', component: EditTagsComponent, outlet:'first'},
                    { path: 'view-report/:id', component: ViewReportComponent, outlet:'first'},
                    { path: 'settings', component: SettingsComponent, outlet:'second'},
                ] },
	

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],

  declarations: []
})

export class AppRoutingModule { }
