import { Component, OnInit } from '@angular/core';
import {SettingsService} from "../service/settings.service";
import {Router} from "@angular/router";
import {User} from "../model/user.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  user: User;
  editForm: FormGroup;
  public data : any;
  private item:any;

  constructor(private formBuilder: FormBuilder,private router: Router, private userService: SettingsService) { }

  ngOnInit() {
    

    this.editForm = this.formBuilder.group({
      id: [],
      username:['', Validators.required],
      email: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      created: ['', Validators.required],
      picture: ['', Validators.required],
      password: ['', Validators.required],
    });

    
  }

  onSubmit() {
    this.userService.updateUser(this.editForm.value.email, this.editForm.value.id).pipe(first()).pipe(first())
      .subscribe(
        data => {
  
          this.router.navigate(['/home', {outlets:{first: ['settings']}}]);
        },
        error => {
            alert(error);
        });
  }


}
