import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { ContactReportsService } from '../service/contact-reports.service';
import { NgbModal, NgbCalendar, NgbDateStruct, NgbDateAdapter, NgbDateNativeAdapter } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import 	toastr from 'toastr';
import 'toastr/build/toastr.min.css';
import { ApiUrl } from '../api-url';
import { AuthConfig } from '../auth-config';
import { uid_request } from '../configrequest';


import { DropzoneComponent , DropzoneDirective,
    DropzoneConfigInterface } from 'ngx-dropzone-wrapper';


@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    providers: [ NgbModal, AuthConfig, {provide: NgbDateAdapter, useClass: NgbDateNativeAdapter}]
})

export class HomeComponent implements OnInit {

    data: any;
    closeResult: string;
    uploadForm: FormGroup;
    tagsList: any;
    tagSelected: string;
    modalRef: any;
    marketer: string;
    dateOfMeeting: any;
    contactImages: any = [];
    isLoading = true;
    UID_REQ: any;
    model: NgbDateStruct;
    filterQuery = '';
    fileOrigin: any;
    contactText: any;
    modal_id:any;
    chartMonthly:any;
    chartTotal:any;
    uploadError= false;
    uploadErrorMsg: string;
    errorFile=[];


    public config: DropzoneConfigInterface = {
        clickable: true,
        maxFiles: 1,
        autoReset: null,
        errorReset: null,
        cancelReset: null,
        uploadMultiple: false,
        url: ApiUrl + 'uploader/submit',
        headers: {
            UUID: uid_request()
        }
    };


    public configMultiple: DropzoneConfigInterface = {
        clickable: true,
        uploadMultiple: true,
        autoReset: null,
        maxFiles: 500,
        url: ApiUrl + 'uploader/multiple',
        headers: {
            UUID: uid_request()
        }
    };

    @ViewChild(DropzoneComponent) componentRef?: DropzoneComponent;
    @ViewChild(DropzoneDirective) directiveRef?: DropzoneDirective;

    constructor(
        private router: Router,
        private contactService: ContactReportsService,
        private modalService: NgbModal,
        private form: FormBuilder,
        public authConf: AuthConfig,
        private calendar: NgbCalendar
    ) {
        if (!this.authConf.isLoggedin()) {
            this.router.navigate(['/']);
        }
    }

    afuConfig = {
      uploadAPI: {
        url: ApiUrl + 'uploader/multiple',
        headers: {
          UUID: uid_request()
        }
      },
      multiple : true,
      hideResetBtn: true,
      formatsAllowed: '.pdf,.doc,.docx',
    };



    ngOnInit() {

        this.UID_REQ = uid_request();
        this.configMultiple.headers.UUID = this.UID_REQ;
        this.afuConfig.uploadAPI.headers.UUID = this.UID_REQ;
        this.uploadForm = this.form.group({
            title: ['', Validators.required],
            purpose: ['', [Validators.required]],
            marketer: ['', [Validators.required]],
            date : ['', [Validators.required]]
        });
        this.contactService.getRecent().subscribe(
            data => {
                this.data = data['body']['data'];
                this.chartMonthly = data['body']['chart']['monthly'];
                this.chartTotal = data ['body']['chart']['all_time'];
            }
        );

        this.contactService.getTags().subscribe(
            data => {
                this.tagsList = data['body'];
                this.isLoading = false;
            }
        );
    }

    get f() {
        return this.uploadForm.controls;
    }

    open(content, ids= null) {
        if (ids !== null) {
          this.modal_id = ids
        }
        this.modalRef = this.modalService.open(content);
    }

    openMultiple(multiples) {
        this.modalRef = this.modalService.open(multiples,{ backdrop: 'static', size: 'lg' });
    }

    onItemSelect(data: {value: string[]}) {
        this.tagSelected = data.value.join(',');
    }

    onUploadSuccess(files) {
        const data = files[1].parse;

        this.uploadForm.controls['title'].setValue(data.title);
        this.uploadForm.controls['purpose'].setValue(data.purpose);
        this.uploadForm.controls['marketer'].setValue(data.marketer);
        this.dateOfMeeting  = new Date(data.date);
        this.fileOrigin = files[1].file_origin;
        this.contactText = data.full_text;
        this.contactImages = files[1].images.join(',');
    }

    DocUpload(files) {
        const data = JSON.parse(files.response);
        if ( data['status'] === true ) {
          toastr.success('Success', data['message']);
          this.isLoading = true;
          this.modalRef.close();
          this.ngOnInit();
        }else{
          this.uploadError = true;
          this.uploadErrorMsg = data['message'];
          this.errorFile = data['data']['file'];
          this.isLoading = true;
          this.modalRef.close();
          this.ngOnInit();
        }
    }


    previewContact(id) {

        this.router.navigate(['/dahsboard', {
            outlets: {
                first: ['view-report', id]
            }
        }]);
    }

  delete_contact() {
    this.isLoading = true;
    this.contactService.delete(this.modal_id).subscribe(
      data => {
        this.ngOnInit();
        this.modalRef.close()
        toastr.success('Success', data['message']);
      },
      error => {
        this.ngOnInit();
        this.modalRef.close();
      });

  }



    formatDate(date) {
        let d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) { month = '0' + month; }
        if (day.length < 2) { day = '0' + day; }

        return [year, month, day].join('-');
    }

    insert(data) {
        this.uploadForm.value.tags      = this.tagSelected;
        this.uploadForm.value.images    = this.contactImages;
        this.uploadForm.value.contact   = this.contactText;
        this.uploadForm.value.file_origin = this.fileOrigin;
        this.uploadForm.value.date        = this.formatDate(this.dateOfMeeting);

        const post_data = this.uploadForm.value;


        this.isLoading = true;

        this.contactService.insert(post_data).subscribe(
            data => {
                toastr.success('Success', data['message']);
                this.contactImages = '';
                this.uploadForm.value.tags = '';
                this.ngOnInit();
                this.modalRef.close();
            }
        );
    }

}
