import * as uuid from 'uuid';


export function uid_request(): any{

    let uuids;

    if (localStorage.getItem("token") != null) {
        let myItem = localStorage.getItem('token');
        const MY_NAMESPACE = JSON.parse(myItem)['token'].toString();
        uuids = uuid.v4(JSON.parse(myItem)['email'].toString(), MY_NAMESPACE);
    }else{
    	uuids = uuid.v4("Sembcorp", Date.now());
	}


	return uuids;
}