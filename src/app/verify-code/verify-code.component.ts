import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import {ForgetPasswordService} from "../service/forget-password.service";

@Component({
  selector: 'app-verify-code',
  templateUrl: './verify-code.component.html',
  styleUrls: ['./verify-code.component.css']
})
export class VerifyCodeComponent implements OnInit {
	forgotForm: FormGroup;
    loading = false;
    submitted = false;

  constructor(private formBuilder: FormBuilder,private router: Router, private forgetService: ForgetPasswordService) { }

  ngOnInit() {
  		this.forgotForm = this.formBuilder.group({
             email: ['', Validators.required],
             code: ['', Validators.required],
        });

  }

   onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.forgotForm.invalid) {
            return;
        }

       this.forgetService.verify_code(this.forgotForm.value.email,this.forgotForm.value.code ).pipe(first()).pipe(first())
      .subscribe(
        data => {
          
        },
        error => {
            alert(error);
        });

    }

}
