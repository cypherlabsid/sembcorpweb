import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable,of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://35.197.138.252/api.sembcorp.com.sg/index.php/auth/registration';

  register(firstname: string, lastname: string, username:string, password:string, email:string ) {
  	
  	const body = new HttpParams()
      .set('username', username)
      .set('first_name', firstname)
      .set('last_name',lastname)
      .set('email',email)
      .set('password', password);
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.http.post(this.baseUrl, body.toString() , { headers, observe: 'response' });
  }


}
