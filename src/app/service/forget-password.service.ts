import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { ApiUrl } from '../api-url';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

@Injectable({
  providedIn: 'root'
})

export class ForgetPasswordService {

	URL = ApiUrl + 'auth/verify_email_adm';  // URL to web api
	URL_Code = ApiUrl + 'auth/verification_code';

	constructor(private http: HttpClient) { }

	  verify_email(email: string) {

		  	const body = new HttpParams()
		      .set('email', email);
		    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

		    return this.http.post(this.URL, body.toString() , { headers, observe: 'response' });
	  }

	   verify_code(email: string, code: string) {
	   		alert(email + ' ' + code);
		  	const body = new HttpParams()
		  	  .set('email', email)
		      .set('verification_code', code);
		    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

		    return this.http.post(this.URL_Code, body.toString() , { headers, observe: 'response' });
	  }

	  post_password (uid: string, data: any) {
      const body = new HttpParams()
        .set('new_password', data)
        .set('user_id', uid);
      const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
      return this.http.post(ApiUrl + 'auth/reset_password_adm', body.toString(), {headers, observe: 'response' });
    }
}
