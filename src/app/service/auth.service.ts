import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders, HttpParams } from '@angular/common/http';
import { ApiUrl } from '../api-url';


@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private http: HttpClient) { }

  baseUrl = ApiUrl + 'admin_auth/login';


  login(username: string, password: string) {
    const body = new HttpParams()
      .set('username', username)
      .set('password', password);
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.http.post(this.baseUrl, body.toString() , { headers, observe: 'response' });
  }

  logout() {
      localStorage.removeItem('token');
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

}
