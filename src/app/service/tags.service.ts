import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders, HttpParams} from '@angular/common/http';
import { Config } from '../config';
import { ApiUrl } from '../api-url';

import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class TagsService {
    config = new Config;
    myItem = localStorage.getItem('token');
    private handleError: HandleError;
    baseUrl = ApiUrl + 'search_tag/';
    getTags = this.baseUrl + 'get';
    delTags = this.baseUrl + 'del';
    addTags = this.baseUrl + 'add';

    constructor( private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
      this.handleError = httpErrorHandler.createHandleError('HeroesService');
    }

    searchTags(tags: string) {
      const body = [{'label': tags}];

      const headers = {headers: new HttpHeaders({ 'Authorization': JSON.parse(this.myItem)['token'].toString() })};
      return this.http.post(this.baseUrl, JSON.stringify(body) , headers);
    }

    tagsLoad() {
      const headers = {headers: new HttpHeaders({ 'Authorization': '' + JSON.parse(this.myItem)['token'].toString() })};
      return this.http.get(this.getTags, headers)
      .pipe(
            catchError(this.handleError('response', {status: false})));
    }

    input_tags(tags: string) {
      const body = new HttpParams().set('tags', tags);

      const headers = {headers: new HttpHeaders({ 'Authorization': '' + JSON.parse(this.myItem)['token'].toString() })};
      return this.http.post(this.baseUrl + 'add', body , headers)
      .pipe(catchError(this.handleError('response', {status: false})));
    }

    deleteTags(id: string) {
      const headers = {headers: new HttpHeaders({ 'Authorization': '' + JSON.parse(this.myItem)['token'].toString() } )};
      return this.http.delete(this.delTags + '/' + id, headers);
    }

    update_tags(tags: string) {

      const myObj = JSON.parse(localStorage.getItem('tags'));
      const tags_id = myObj['tags_id'];


      const body = new HttpParams().set('tags', tags);
      const headers = {headers: new HttpHeaders({ 'Authorization': '' + JSON.parse(this.myItem)['token'].toString(), 'Content-Type': 'application/x-www-form-urlencoded' })};
      return this.http.put(this.baseUrl + 'update' + '/' + tags_id, body.toString() , headers);

    }

}
