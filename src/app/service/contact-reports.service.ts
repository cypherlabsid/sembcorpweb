import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders, HttpParams } from '@angular/common/http';
import { ApiUrl } from '../api-url';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class ContactReportsService {

  constructor(private http: HttpClient) { }

  myItem    = localStorage.getItem('token');
  baseUrl   = ApiUrl + 'contactreport/recent';
  insertUrl = ApiUrl + 'contactreport/';

  load_recent_add = ApiUrl + 'contactreport/recent_add';
  get_tags        = ApiUrl + 'search_tag/all';
  url_report      = ApiUrl + 'contactreport/preview?';
  url_delete_report = ApiUrl + 'contactreport/delete';

  public get_name_user: string;

  getData() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': '' + JSON.parse(this.myItem)['token'].toString(),
      'Access-Control-Allow-Credentials' : 'true'
    });
    return this.http.get(this.insertUrl + 'all', { headers, observe: 'response' });
  }


  getRecent() {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': '' + JSON.parse(this.myItem)['token'].toString(),
      'Access-Control-Allow-Credentials' : 'true'
    });

    return this.http.get(this.insertUrl + 'recent_web', { headers, observe: 'response' });
  }

   getLast() {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': '' + JSON.parse(this.myItem)['token'].toString(),
      'Access-Control-Allow-Credentials' : 'true'
    });
    const rec = localStorage.getItem('_recent');
    let id = [];
    if(rec !== null){

      id = JSON.parse(rec)
      if (!Array.isArray(id)) {
        id = [];
      }
    }

    return this.http.get(this.insertUrl + 'last_view?id=' + id.join(','), { headers, observe: 'response' });
  }

  addData(tags: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': '' + JSON.parse(this.myItem)['token'].toString()
    });


    const body = new HttpParams().set('tag', tags);
    return this.http.post(this.baseUrl, body.toString() , { headers, observe: 'response' });
  }
   getUserByAdd() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': '' + JSON.parse(this.myItem)['token'].toString(),
      'Access-Control-Allow-Credentials' : 'true'
    });

    return this.http.get(this.load_recent_add, { headers, observe: 'response' });
  }

   updateTags(tag: string, id: number) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    const body = new HttpParams().set('tag', tag);
    return this.http.put(this.baseUrl + '/' + id, body.toString() , { headers, observe: 'response' });
  }

  deleteTags(id: number) {
    const headers = {headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })};
    return this.http.delete(this.baseUrl + '/' + id, headers);
  }

  insert(data: any) {

    const body = new HttpParams().set('title', data.title)
    .set('purpose', data.purpose)
    .set('contact', data.contact)
    .set('images', data.images)
    .set('tags', data.tags)
    .set('marketer', data.marketer).set('date', data.date).set('file_origin', data.file_origin);

    const MY_NAMESPACE = JSON.parse(this.myItem)['token'].toString();
    const uuids = uuid.v4(JSON.parse(this.myItem)['email'].toString(), MY_NAMESPACE);


    const headers = {headers: new HttpHeaders({ 'Authorization': '' + JSON.parse(this.myItem)['token'].toString(), 'UUID': uuids })};
    return this.http.post(this.insertUrl + 'single_contact', body , headers);
  }

  getTags() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': '' + JSON.parse(this.myItem)['token'].toString(),
      'Access-Control-Allow-Credentials' : 'true'
    });
    return this.http.get(this.get_tags, { headers, observe: 'response' });
  }

  view_report(id: number) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': '' + JSON.parse(this.myItem)['token'].toString(),
      'Access-Control-Allow-Credentials' : 'true'
    });

    return this.http.get(this.url_report + 'id=' + id, { headers, observe: 'response' });
  }

  update_report(data: any) {
     const body = new HttpParams()
    .set('title', data.title)
    .set('purpose', data.purpose)
    .set('tags', data.tags)
    .set('marketer', data.marketer)
    .set('date', data.tgl)
    .set('id', data.id);


    const headers = {headers: new HttpHeaders({ 'Authorization': '' + JSON.parse(this.myItem)['token'].toString()})};
    return this.http.put(this.insertUrl + 'update', body , headers);
  }

  delete(id: string) {
    const headers = {headers: new HttpHeaders({ 'Authorization': '' + JSON.parse(this.myItem)['token'].toString() } )};
    return this.http.delete(this.url_delete_report + '/' + id, headers);
  }

}
