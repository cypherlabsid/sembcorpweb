import { TestBed } from '@angular/core/testing';

import { ContactReportsService } from './contact-reports.service';

describe('ContactReportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContactReportsService = TestBed.get(ContactReportsService);
    expect(service).toBeTruthy();
  });
});
