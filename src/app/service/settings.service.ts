import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import {User} from "../model/user.model";


@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  myItem = localStorage.getItem('token');

  constructor(private http: HttpClient) { }

  baseUrl = 'http://35.197.138.252/api.sembcorp.com.sg/index.php/accounts/update_profile';
  
  getData(){
    const headers = new HttpHeaders({ 'Authorization':''+JSON.parse(this.myItem)['token'].toString() });

    return this.http.get(this.baseUrl, { headers, observe: 'response' });
  }

   updateUser(first_name:string, last_name:string) {
    const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded', 'Authorization':''+JSON.parse(this.myItem)['token'].toString() });
    const body = new HttpParams().
                set('first_name', first_name).
                set('last_name', last_name).
                set('regional','Indonesia');

    
    localStorage.setItem('first_name', first_name);
    localStorage.setItem('last_name', last_name);

    return this.http.post(this.baseUrl, body.toString() , { headers, observe: 'response' });
  }

  deleteUser(id: number) {
    const headers = {headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })};
     
    return this.http.delete(this.baseUrl + '/' + id, headers);
  }

}
